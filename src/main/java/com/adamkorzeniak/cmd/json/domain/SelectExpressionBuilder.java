package com.adamkorzeniak.cmd.json.domain;

import lombok.Value;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Value
public class SelectExpressionBuilder {
    private final Set<String> included = new HashSet<>();
    private final Set<String> excluded = new HashSet<>();

    public SelectExpressionBuilder include(String key) {
        this.included.add(key);
        return this;
    }

    public SelectExpressionBuilder exclude(String key) {
        this.excluded.add(key);
        return this;
    }

    public SelectExpression build() {
        validate();
        clean();

        Set<SelectExpression.Item> items = new HashSet<>();
        included.stream()
                .map((key -> new SelectExpression.Item(key, true)))
                .forEach(items::add);
        excluded.stream()
                .map((key -> new SelectExpression.Item(key, false)))
                .forEach(items::add);
        return new SelectExpression(items);
    }

    private void validate() {
        Set<String> bothOptions = included.stream()
                .filter(excluded::contains)
                .collect(Collectors.toSet());
        if (!bothOptions.isEmpty()) {
            throw new RuntimeException("You cannot both include and exclude fields. Issue occured for: " + bothOptions);
        }
    }

    private void clean() {
        //TODO:
    }

}
